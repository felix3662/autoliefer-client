export class Receipt {
    id: number;
    ordernr: string;
    date: Date;
    amount: number;
    annotation: string;
    owner: number;
}