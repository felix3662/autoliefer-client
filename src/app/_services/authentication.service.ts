﻿import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {User} from '@/_models';
import {error} from "@angular/compiler/src/util";


@Injectable({providedIn: 'root'})
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    private auth = "OrdixSampleID:ordixSecret";
    // @ts-ignore
    private encodedAuth = btoa(this.auth);
    private authHeader = "Basic " + new String(this.encodedAuth);
    private httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded',
            'authorization': this.authHeader,
        })
    };
    private tokenBody = {
        params: new HttpParams()
            .set('grant_type', 'password')
            .set('username', 'felix')
            .set('password', 'password')
            .set('scope', 'user_info')
    };
    status: any;
    private headers: string[];
    private config;
    private token;


    constructor(private http: HttpClient) {
        // @ts-ignore
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    getToken() {
        this.http.post('http://localhost:8081/oauth/token', this.tokenBody.params, {headers: this.httpOptions.headers}).subscribe(
            (val) => {
                //console.log("POST call successful value returned in body",
                //    val['access_token']);
                this.token = val['access_token'];

            });
        return this.token;

    }

    login(username, password) {

        return this.http.post<any>(`${config.apiUrl}/user/login`, {username, password})
            .pipe(map(user => {
                if (user != null) {
                    // @ts-ignore
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                    return user;
                } else {
                    error("\nUser does not exist or password is wrong");
                }
            }));

    }

    logout() {
        // remove user from local storage and set current user to null
        // @ts-ignore
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

    getCurrentUserId(){
        return this.currentUserValue.id;
    }
}