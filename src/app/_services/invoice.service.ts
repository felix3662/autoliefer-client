import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {AuthenticationService} from "@/_services/authentication.service";
import {Invoice} from "@/_models/invoice";


@Injectable({ providedIn: 'root' })
export class InvoiceService {
    constructor(
        private http: HttpClient,
        private authService : AuthenticationService,
    ) {}
    private httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
        })
    };

    createInvoice(invoice : Invoice) {
        invoice.owner = this.authService.getCurrentUserId();
        return this.http.post(`http://localhost:8084/invoice/create`, invoice, this.httpOptions);
    }
}
