import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Receipt} from "@/_models/receipt";
import {AuthenticationService} from "@/_services/authentication.service";


@Injectable({ providedIn: 'root' })
export class ReceiptService {
    constructor(
        private http: HttpClient,
        private authService : AuthenticationService,
    ) {}

    createReceipt(receipt : Receipt) {
        receipt.owner = this.authService.getCurrentUserId();
        return this.http.post(`http://localhost:8083/receipt/create`, receipt);
    }
}