﻿import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {User} from '@/_models';


@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }
    private auth = "OrdixSampleID:ordixSecret";
    // @ts-ignore
    private encodedAuth = btoa(this.auth);
    private authHeader = "Basic " + new String(this.encodedAuth);

    private httpOptions = {

        headers: new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded',
            'authorization': this.authHeader,
        })
    };

    register(user: User) {
        return this.http.post(`${config.apiUrl}/user/create`, user);
    }

    getToken(){
         let tokenBody = {
            'scope': 'user_info',
            'username': 'felix',
            'password': 'password',
            'grant_type':'password',

        };

        return this.http.post('http:/localhost:8081/oauth/token',tokenBody,this.httpOptions)
    }

    delete(id: number) {
        return this.http.delete(`${config.apiUrl}/user/delete/${id}`);
    }
}