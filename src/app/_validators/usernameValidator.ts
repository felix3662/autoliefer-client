import {Injectable} from '@angular/core';
import {FormControl} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {User} from '@/_models';

@Injectable()
export class UsernameValidator {

    debouncer: any;

    constructor(private http: HttpClient) {

    }

    checkUsername(control: FormControl): any {

        clearTimeout(this.debouncer);

        return new Promise(resolve => {

            this.debouncer = setTimeout(() => {

                this.validateUsername(control.value).subscribe((res) => {
                    if(res.username == control.value){
                        resolve({'usernameInUse': true});
                    }
                }, (err) => {
                    resolve(null);
                });

            }, 1000);

        });
    }

    validateUsername(username){
        return this.http.get<User>(`${config.apiUrl}/user/` + username);
    }

}