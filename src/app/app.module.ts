﻿import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {UsernameValidator} from "@/_validators/usernameValidator";
// used to create  backend
import {ErrorInterceptor, JwtInterceptor} from './_helpers';

import {appRoutingModule} from './app.routing';
import {AppComponent} from './app.component';
import {HomeComponent} from './home';
import {LoginComponent} from './login';
import {RegisterComponent} from './register';
import {AlertComponent} from './_components';
import {CreateReceiptComponent} from "@/createreceipt/create-receipt.component";
import {CreateInvoiceComponent} from "@/createinvoice/create-invoice.component";

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        appRoutingModule
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        CreateReceiptComponent,
        CreateInvoiceComponent,
        AlertComponent
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        UsernameValidator

        // provider used to create  backend
        //BackendProvider
    ],
    bootstrap: [AppComponent]
})
export class AppModule { };