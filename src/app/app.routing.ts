﻿import {RouterModule, Routes} from '@angular/router';

import {HomeComponent} from './home';
import {LoginComponent} from './login';
import {RegisterComponent} from './register';
import {AuthGuard} from './_helpers';
import {CreateReceiptComponent} from "@/createreceipt/create-receipt.component";
import {CreateInvoiceComponent} from "@/createinvoice/create-invoice.component";

const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'create-receipt', component: CreateReceiptComponent},
    { path: 'create-invoice', component: CreateInvoiceComponent},

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const appRoutingModule = RouterModule.forRoot(routes);