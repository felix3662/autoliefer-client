import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {AlertService, AuthenticationService} from '@/_services';
import {InvoiceService} from "@/_services/invoice.service";

@Component({ templateUrl: 'create-invoice.component.html' })
export class CreateInvoiceComponent implements OnInit {
    invoiceForm: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService,
        private invoiceService: InvoiceService,
        private alertService: AlertService,
    ) {}

    ngOnInit() {
        this.invoiceForm = this.formBuilder.group({
            ordernr: ['', Validators.required],
            date: ['', Validators.required],
            amount: ['', Validators.required],
            annotation: [''],
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.invoiceForm.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.invoiceForm.invalid) {
            return;
        }

        this.loading = true;
        this.invoiceService.createInvoice(this.invoiceForm.value).subscribe();


        this.router.navigate(["/"]);
        this.alertService.success("Rechnung erfasst", true);
    }

}
