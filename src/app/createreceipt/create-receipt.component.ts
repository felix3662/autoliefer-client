import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {AlertService, AuthenticationService} from '@/_services';
import {ReceiptService} from "@/_services/receipt.service";

@Component({ templateUrl: 'create-receipt.component.html' })
export class CreateReceiptComponent implements OnInit {
    receiptForm: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService,
        private receiptService: ReceiptService,
        private alertService: AlertService,
    ) {}

    ngOnInit() {
        this.receiptForm = this.formBuilder.group({
            ordernr: ['', Validators.required],
            date: ['', Validators.required],
            amount: ['', Validators.required],
            annotation: [''],
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.receiptForm.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.receiptForm.invalid) {
            return;
        }

        this.loading = true;
        this.receiptService.createReceipt(this.receiptForm.value).subscribe();


        this.router.navigate(["/"]);
        this.alertService.success("Lieferschein erfasst", true);
    }

}
